<?php
$this->breadcrumbs=array(
	'Member Activities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MemberActivity', 'url'=>array('index')),
	array('label'=>'Manage MemberActivity', 'url'=>array('admin')),
);
?>

<h1>Create MemberActivity</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>