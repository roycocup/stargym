<?php
$this->breadcrumbs=array(
	'Member Activities'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MemberActivity', 'url'=>array('index')),
	array('label'=>'Create MemberActivity', 'url'=>array('create')),
	array('label'=>'View MemberActivity', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MemberActivity', 'url'=>array('admin')),
);
?>

<h1>Update MemberActivity <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>