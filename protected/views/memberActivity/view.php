<?php
$this->breadcrumbs=array(
	'Member Activities'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MemberActivity', 'url'=>array('index')),
	array('label'=>'Create MemberActivity', 'url'=>array('create')),
	array('label'=>'Update MemberActivity', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MemberActivity', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MemberActivity', 'url'=>array('admin')),
);
?>

<h1>View MemberActivity #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'member_id',
		'activity_id',
	),
)); ?>
