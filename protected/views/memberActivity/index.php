<?php
$this->breadcrumbs=array(
	'Member Activities',
);

$this->menu=array(
	array('label'=>'Create MemberActivity', 'url'=>array('create')),
	array('label'=>'Manage MemberActivity', 'url'=>array('admin')),
);
?>

<h1>Member Activities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
