
<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('Member')); ?>:</b>
	<?php echo CHtml::encode($data->member->first_name." ".$data->member->last_name. " \"". $data->member->nickname."\""); ?>
	<br />
	
	<b><?php echo CHtml::encode($data->getAttributeLabel('payment id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

<!--	<b><?php echo CHtml::encode($data->getAttributeLabel('member_id')); ?>:</b>
	<?php echo CHtml::encode($data->member_id); ?>
	<br />-->

	<b><?php echo CHtml::encode($data->getAttributeLabel('value')); ?>:</b>
	<?php echo CHtml::encode($data->value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('extras')); ?>:</b>
	<?php echo CHtml::encode($data->extras); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('extras_value')); ?>:</b>
	<?php echo CHtml::encode($data->extras_value); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paid on')); ?>:</b>
	<?php echo CHtml::encode($data->created); ?>
	<br />
	<form action="delete/<?php echo $data->id; ?>">
	<input type="submit" value="Delete this payment"/>

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('modified')); ?>:</b>
	<?php echo CHtml::encode($data->modified); ?>
	<br />

	*/ ?>

</div>