<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return CMap::mergeArray(
		require(dirname(__FILE__).'/main.php'), 
		array(
			'modules'=>array(
				// uncomment the following to enable the Gii tool
				'gii'=>array(
					'class'=>'system.gii.GiiModule',
					'password'=>'squid',
					// If removed, Gii defaults to localhost only. Edit carefully to taste.
					'ipFilters'=>array('127.0.0.1','::1'),
				),
			),
			'components'=>array(
				// uncomment the following to use a MySQL database
				'db'=>array(
					'connectionString' => 'mysql:host=localhost;dbname=stargym',
					'emulatePrepare' => true,
					'username' => 'rod',
					'password' => 'pitaonline',
					'charset' => 'utf8',
				),
			),
		)
);