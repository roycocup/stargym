<?php echo CHtml::beginForm($formaction,$formmethod,array('id'=>$formid)); ?>
					<fieldset>
						<legend><?php echo CHtml::encode($legend);?></legend>
						<p><?php echo CHtml::textField($forminputname,$forminputvalue,array('size'=>$forminputsize,'class'=>$forminputclass,'onfocus'=>"this.value = ( this.value == this.defaultValue ) ? '' : this.value;return true;"));?>&nbsp;<?php echo CHtml::submitButton($formsubmitlabel,array('class'=>$formsubmitclass));?></p>
					</fieldset>
<?php echo CHtml::endForm(); ?>

