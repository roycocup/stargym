<ul class="<?php echo CHtml::encode($class);?>">
<?php
foreach($links AS $link)
{
	$active = '';
	if($link['active'] == true)
	{
		$active = ' id="'.CHtml::encode($activeid).'"';
	}
?>
			<li<?php echo $active;?>><?php echo CHtml::link('<span>'.CHtml::encode($link['label']).'</span>',$link['url']);?>
            	<?php 
				if(count($link['submenu']) > 0){ ?>
                <ul>
                <?php
					foreach($link['submenu'] AS $sublink){
						if($sublink['active']) {
							$label = '<span><strong>'.CHtml::encode($sublink['label']).'</strong></span>';
						} else {
							$label = '<span>'.CHtml::encode($sublink['label']).'</span>';
						}
						?>
                    	<li><?php echo CHtml::link($label,$sublink['url']);?></li>
                <?php } ?>
                </ul>
				<?php
				}?>
                	
          </li>
<?php
}
?>
</ul>