<?php
class FlashMessage extends CWidget
{

	public function init()
    {
        // this method is called by CController::beginWidget()
    }

    public function run()
    {
		$all_flashes[] = Yii::app()->user->getFlashes();
		
		//check for flash messages
		foreach ($all_flashes as $flashes){
			if($flashes)
			{ 
				//if they exist then create the scripts and register them
				Yii::app()->clientScript->registerCoreScript('jquery');
				Yii::app()->clientScript->registerScript(
				   'myFlashHideEffect',
				   '$(".flashmsg msg done").animate({opacity: 1.0}, 3000);
					$(".flashmsg msg error").animate({opacity: 1.0}, 3000);
					'
				   ,CClientScript::POS_READY
				);

				//loop through the messages and generate the html
				foreach($flashes AS $category=>$message)
				{
					$this->render('_flashmessage',array(
						'category'=>$category,
						'message'=>$message,
						));
					
				}

			}
		}
		
    }
}
