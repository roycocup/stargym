/*
 Navicat Premium Data Transfer

 Source Server         : Localhost
 Source Server Type    : MySQL
 Source Server Version : 50144
 Source Host           : localhost
 Source Database       : stargym

 Target Server Type    : MySQL
 Target Server Version : 50144
 File Encoding         : utf-8

 Date: 05/02/2012 18:36:43 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `activities`
-- ----------------------------
DROP TABLE IF EXISTS `activities`;
CREATE TABLE `activities` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `activity` varchar(255) NOT NULL,
  `order` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `activities`
-- ----------------------------
BEGIN;
INSERT INTO `activities` VALUES ('1', 'BJJ', '1'), ('2', 'Boxing', '2'), ('3', 'Muay Thay', '4'), ('4', 'Wrestling', '5'), ('5', 'BJJ Kids', '3'), ('6', 'Conditioning', '6'), ('7', 'MMA', '7');
COMMIT;

-- ----------------------------
--  Table structure for `images`
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image_path` varchar(250) NOT NULL,
  `image_name` varchar(250) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `member_activity`
-- ----------------------------
DROP TABLE IF EXISTS `member_activity`;
CREATE TABLE `member_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `activity_id` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(250) DEFAULT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `nickname` varchar(250) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `picture_id` int(11) DEFAULT NULL,
  `next_payment_date` date DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `members`
-- ----------------------------
BEGIN;
INSERT INTO `members` VALUES ('1', 'Tom--Boxing Kid-3bc62', 'Tom', '', 'Boxing Kid', '', '1970-01-01', null, '2012-06-01', '2012-05-02 18:17:47', '2012-05-02 18:17:47'), ('2', 'Reez --H\'S M8-81762', 'Reez ', '', 'H\'S M8', '', '1970-01-01', null, '2012-06-27', '2012-05-02 18:18:35', '2012-05-02 18:18:35'), ('3', 'Babos---09b99', 'Babos', '', '', '', '1970-01-01', null, '2012-06-30', '2012-05-02 18:18:55', '2012-05-02 18:18:55'), ('4', 'Lex-Luthor-Muay Thay-96cc0', 'Lex', 'Luthor', 'Muay Thay', '', '1970-01-01', null, '2012-06-30', '2012-05-02 18:19:32', '2012-05-02 18:19:32'), ('5', 'Ed-Murph-Lunchtime Negao-2215b', 'Ed', 'Murph', 'Lunchtime Negao', '', '1970-01-01', null, '2012-06-30', '2012-05-02 18:19:57', '2012-05-02 18:19:57'), ('6', 'Anton---d6d41', 'Anton', '', '', '', '1970-01-01', null, '2012-06-29', '2012-05-02 18:20:16', '2012-05-02 18:20:16'), ('7', 'Lola-Nego-Fresco-6d212', 'Lola', 'Nego', 'Fresco', '', '1970-01-01', null, '2012-06-28', '2012-05-02 18:20:37', '2012-05-02 18:20:37'), ('8', 'David--Gordinho-83fbe', 'David', '', 'Gordinho', '', '1970-01-01', null, '2012-06-27', '2012-05-02 18:20:50', '2012-05-02 18:20:50'), ('9', 'Professional-Fighter--a448d', 'Professional', 'Fighter', '', '', '1970-01-01', null, '2012-06-20', '2012-05-02 18:21:11', '2012-05-02 18:21:11'), ('10', 'H--Boxing Omids-1dca7', 'H', '', 'Boxing Omids', '', '1970-01-01', null, '2012-06-25', '2012-05-02 18:21:38', '2012-05-02 18:21:38'), ('11', 'Sakuraba---71354', 'Sakuraba', '', '', '', '1970-01-01', null, '2012-06-25', '2012-05-02 18:21:54', '2012-05-02 18:21:54'), ('12', 'Stefano--Italiano (lunchtime)-59094', 'Stefano', '', 'Italiano (lunchtime)', '', '1970-01-01', null, '2012-06-25', '2012-05-02 18:22:54', '2012-05-02 18:22:54'), ('13', 'Giovanne--morning class-accde', 'Giovanne', '', 'morning class', '', '1970-01-01', null, '2012-06-25', '2012-05-02 18:23:15', '2012-05-02 18:23:15'), ('14', 'Charlie--Lunchtime-ac40f', 'Charlie', '', 'Lunchtime', '', '1970-01-01', null, '2012-06-24', '2012-05-02 18:23:38', '2012-05-02 18:23:38'), ('15', 'Pianoman---31d8a', 'Pianoman', '', '', '', '1970-01-01', null, '2012-06-23', '2012-05-02 18:23:53', '2012-05-02 18:23:53'), ('16', 'Robert-Polaco-Muay Thay-e7e38', 'Robert', 'Polaco', 'Muay Thay', '', '1970-01-01', null, '2012-06-23', '2012-05-02 18:24:29', '2012-05-02 18:24:29'), ('17', 'Cabeleira--Lunchtime-c7423', 'Cabeleira', '', 'Lunchtime', '', '1970-01-01', null, '2012-06-23', '2012-05-02 18:24:42', '2012-05-02 18:24:42'), ('18', 'Mark--dave-3cfe2', 'Mark', '', 'dave', '', '1970-01-01', null, '2012-06-23', '2012-05-02 18:25:14', '2012-05-02 18:25:14'), ('19', 'Jacopo---7443b', 'Jacopo', '', '', '', '1970-01-01', null, '2012-06-20', '2012-05-02 18:25:25', '2012-05-02 18:25:25'), ('20', 'Lucio---eac1c', 'Lucio', '', '', '', '1970-01-01', null, '2012-06-20', '2012-05-02 18:25:39', '2012-05-02 18:25:39'), ('21', 'Young-Gary--ee0a2', 'Young', 'Gary', '', '', '1970-01-01', null, '2012-06-19', '2012-05-02 18:26:11', '2012-05-02 18:26:11'), ('22', 'Stefano--Bulgarian-03f48', 'Stefano', '', 'Bulgarian', '', '1970-01-01', null, '2012-06-19', '2012-05-02 18:26:32', '2012-05-02 18:26:32'), ('23', 'Emmanuel---26b07', 'Emmanuel', '', '', '', '1970-01-01', null, '2012-06-06', '2012-05-02 18:26:43', '2012-05-02 18:26:43'), ('24', 'Rodrigo-Dias-Trepeiro-6420a', 'Rodrigo', 'Dias', 'Trepeiro', '', '1976-09-10', null, '2012-06-15', '2012-05-02 18:27:10', '2012-05-02 18:27:10'), ('25', 'Nick--Blue belt-93c48', 'Nick', '', 'Blue belt', '', '1970-01-01', null, '2012-06-17', '2012-05-02 18:27:29', '2012-05-02 18:27:29'), ('26', 'Helena--MMA-811d6', 'Helena', '', 'MMA', '', '1970-01-01', null, '2012-06-16', '2012-05-02 18:27:46', '2012-05-02 18:27:46'), ('27', 'David--Kids-800ac', 'David', '', 'Kids', '', '1970-01-01', null, '2012-06-17', '2012-05-02 18:28:00', '2012-05-02 18:28:00'), ('28', '5 months---18777', '5 months', '', '', '', '1970-01-01', null, '2012-06-16', '2012-05-02 18:28:15', '2012-05-02 18:28:15'), ('29', 'Little Giant---aa81d', 'Little Giant', '', '', '', '1970-01-01', null, '2012-06-16', '2012-05-02 18:28:33', '2012-05-02 18:28:33'), ('30', 'Lukas--Polaco-3d541', 'Lukas', '', 'Polaco', '', '1970-01-01', null, '2012-06-13', '2012-05-02 18:28:42', '2012-05-02 18:28:42'), ('31', 'Xaverinho---51f98', 'Xaverinho', '', '', '', '1970-01-01', null, '2012-06-12', '2012-05-02 18:29:34', '2012-05-02 18:29:34'), ('32', 'Lex-Luthor-BJJ-eb4fd', 'Lex', 'Luthor', 'BJJ', '', '1970-01-01', null, '2012-06-12', '2012-05-02 18:29:55', '2012-05-02 18:29:55'), ('33', 'Innes---2596a', 'Innes', '', '', '', '1970-01-01', null, '2012-06-11', '2012-05-02 18:30:04', '2012-05-02 18:30:04');
COMMIT;

-- ----------------------------
--  Table structure for `payments`
-- ----------------------------
DROP TABLE IF EXISTS `payments`;
CREATE TABLE `payments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(10) unsigned NOT NULL,
  `value` int(11) NOT NULL,
  `extras` varchar(255) DEFAULT NULL,
  `extras_value` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `member_id` (`member_id`),
  CONSTRAINT `payments_ibfk_1` FOREIGN KEY (`member_id`) REFERENCES `members` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `teachers`
-- ----------------------------
DROP TABLE IF EXISTS `teachers`;
CREATE TABLE `teachers` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(200) DEFAULT NULL,
  `phone` varchar(250) DEFAULT NULL,
  `facebook` varchar(250) DEFAULT NULL,
  `next_wage_date` date DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `teachers`
-- ----------------------------
BEGIN;
INSERT INTO `teachers` VALUES ('4', 'Marco', 'Canha', null, null, null, '2012-02-11', '2012-02-01 18:28:38', '2012-02-01 18:28:41'), ('5', 'Luiz', 'Ribeiro', 'porradalondres@hotmail.com', '', '', '2012-02-28', '2012-02-11 14:20:05', '2012-02-11 14:20:05'), ('6', 'Diego', 'Vital', '', '', '', '2012-02-21', '2012-02-14 12:47:02', '2012-02-14 12:47:02'), ('7', 'Nick', 'Forrer', '', '', '', '2012-02-21', '2012-02-14 12:47:20', '2012-02-14 12:47:20');
COMMIT;

-- ----------------------------
--  Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(150) NOT NULL,
  `password` varchar(10) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `wages`
-- ----------------------------
DROP TABLE IF EXISTS `wages`;
CREATE TABLE `wages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` int(10) unsigned NOT NULL,
  `amount` int(20) DEFAULT NULL,
  `payed` int(1) NOT NULL,
  `set_date` date DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `teacher_id` (`teacher_id`),
  CONSTRAINT `wages_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `teachers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Records of `wages`
-- ----------------------------
BEGIN;
INSERT INTO `wages` VALUES ('1', '4', '50', '1', '2012-02-01', '2012-02-01 18:41:35', '2012-02-01 18:41:35'), ('2', '4', '56', '1', '2012-02-02', '2012-02-01 18:42:29', '2012-02-01 18:42:29'), ('3', '4', '70', '0', '2012-02-02', '2012-02-01 18:42:44', '2012-02-01 18:42:44'), ('4', '5', '10', '1', '2012-02-14', '2012-02-14 12:05:12', '2012-02-14 12:05:12'), ('5', '7', '500', '1', '2012-02-14', '2012-02-14 12:47:43', '2012-02-14 12:47:43');
COMMIT;

